package cleaner

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/Sirupsen/logrus"
	homedir "github.com/mitchellh/go-homedir"
)

func GetDefaultMachinesDirectory() (dir string) {
	dir, err := homedir.Expand("~/.docker/machine/machines")
	if err != nil {
		logrus.Fatalf("Error during discovering default machines directory: %v", err)
	}
	return
}

type MachinesFinderInterface interface {
	ListMachines() ([]MachineInterface, error)
}

type MachinesFinder struct {
	machinesDirectory string
}

type NotAMachineError struct {
	err error
}

func (n *NotAMachineError) Error() string {
	return n.err.Error()
}

func (m *MachinesFinder) readConfigFile(dir string, file os.FileInfo) (machine MachineInterface, err error) {
	filePath := filepath.Join(dir, file.Name())

	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	type machineDetailsDriver struct {
		DropletID int64 `json:"DropletID"`
	}

	var machineDetails struct {
		Driver machineDetailsDriver `json:"Driver"`
		Name   string               `json:"Name"`
	}

	err = json.Unmarshal(content, &machineDetails)
	if err != nil {
		return
	}

	machine = &Machine{
		name:       machineDetails.Name,
		dropletID:  machineDetails.Driver.DropletID,
		modifiedAt: file.ModTime(),
		directory:  dir,
	}

	return
}

func (m *MachinesFinder) readConfig(dir os.FileInfo) (machine MachineInterface, err error) {
	machineDirectory := filepath.Join(m.machinesDirectory, dir.Name())
	machineFiles, err := ioutil.ReadDir(machineDirectory)
	if err != nil {
		return
	}

	for _, file := range machineFiles {
		if file.Name() == "config.json" {
			machine, err = m.readConfigFile(machineDirectory, file)

			return
		}
	}

	err = &NotAMachineError{err: fmt.Errorf("Couldn't find 'config.json' file for machine '%s'", dir.Name())}

	return
}

func (m *MachinesFinder) ListMachines() (machines []MachineInterface, err error) {
	logrus.Debugf("Looking for machines in '%s'", m.machinesDirectory)
	entries, err := ioutil.ReadDir(m.machinesDirectory)
	if err != nil {
		return
	}

	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		}

		var machine MachineInterface
		machine, err = m.readConfig(entry)
		if err != nil {
			if _, ok := err.(*NotAMachineError); ok {
				logrus.Warnf("Skipping machine directory because of: %v", err)
				err = nil
				continue
			}
			return
		}

		machines = append(machines, machine)
	}

	return
}

func NewMachinesFinder(machinesDirectory string) *MachinesFinder {
	return &MachinesFinder{
		machinesDirectory: machinesDirectory,
	}
}
