package cleaner

import (
	"bytes"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func testClearNoMachines(t *testing.T, buf *bytes.Buffer) {
	mf := &MockMachinesFinderInterface{}
	defer mf.AssertExpectations(t)

	mf.On("ListMachines").Once().Return([]MachineInterface{}, nil)

	cleaner := NewDropletZeroMachinesCleaner(mf, 10)
	cleaner.Clean()

	assert.Contains(t, buf.String(), "Starting machines cleanup")
	assert.Contains(t, buf.String(), "Number of found machines: 0")
	assert.Contains(t, buf.String(), "Finished machines cleanup. Removed 0 machines")
	assert.NotContains(t, buf.String(), "Will remove: machine1")
	assert.NotContains(t, buf.String(), "Will remove: machine2")
	assert.NotContains(t, buf.String(), `Error while removing machine`)
}

func testClearRemovableMachines(t *testing.T, buf *bytes.Buffer) {
	mf := &MockMachinesFinderInterface{}
	defer mf.AssertExpectations(t)
	machine1 := &MockMachineInterface{}
	defer machine1.AssertExpectations(t)
	machine2 := &MockMachineInterface{}
	defer machine2.AssertExpectations(t)

	modifiedAt := time.Now().Add(-20 * time.Second)

	machine1.On("DropletID").Once().Return(int64(0))
	machine1.On("ModifiedAt").Once().Return(modifiedAt)
	machine1.On("Name").Once().Return("machine1")
	machine1.On("ModifiedAt").Once().Return(modifiedAt)
	machine1.On("Delete").Once().Return(nil)

	machine2.On("DropletID").Once().Return(int64(0))
	machine2.On("ModifiedAt").Once().Return(modifiedAt)
	machine2.On("Name").Once().Return("machine2")
	machine2.On("ModifiedAt").Once().Return(modifiedAt)
	machine2.On("Delete").Once().Return(nil)

	machines := []MachineInterface{
		machine1,
		machine2,
	}
	mf.On("ListMachines").Once().Return(machines, nil)

	cleaner := NewDropletZeroMachinesCleaner(mf, 10)
	cleaner.EnableDelete()
	cleaner.Clean()

	assert.Contains(t, buf.String(), "Starting machines cleanup")
	assert.Contains(t, buf.String(), "Number of found machines: 2")
	assert.Contains(t, buf.String(), "Finished machines cleanup. Removed 2 machines")
	assert.Contains(t, buf.String(), "Will remove: machine1")
	assert.Contains(t, buf.String(), "Will remove: machine2")
	assert.NotContains(t, buf.String(), `Error while removing machine`)

	assert.Equal(t, int64(2), cleaner.totalNumberOfRemovedMachines)
	assert.Equal(t, int64(0), cleaner.totalNumberOfRemoveMachineErrors)
}

func testClearRemovableMachinesWithDeleteDisabled(t *testing.T, buf *bytes.Buffer) {
	mf := &MockMachinesFinderInterface{}
	defer mf.AssertExpectations(t)
	machine1 := &MockMachineInterface{}
	defer machine1.AssertExpectations(t)
	machine2 := &MockMachineInterface{}
	defer machine2.AssertExpectations(t)

	modifiedAt := time.Now().Add(-20 * time.Second)

	machine1.On("DropletID").Once().Return(int64(0))
	machine1.On("ModifiedAt").Once().Return(modifiedAt)
	machine1.On("Name").Once().Return("machine1")
	machine1.On("ModifiedAt").Once().Return(modifiedAt)

	machine2.On("DropletID").Once().Return(int64(0))
	machine2.On("ModifiedAt").Once().Return(modifiedAt)
	machine2.On("Name").Once().Return("machine2")
	machine2.On("ModifiedAt").Once().Return(modifiedAt)

	machines := []MachineInterface{
		machine1,
		machine2,
	}
	mf.On("ListMachines").Once().Return(machines, nil)

	cleaner := NewDropletZeroMachinesCleaner(mf, 10)
	cleaner.Clean()

	assert.Contains(t, buf.String(), "Starting machines cleanup")
	assert.Contains(t, buf.String(), "Number of found machines: 2")
	assert.Contains(t, buf.String(), "Finished machines cleanup. Removed 0 machines")
	assert.Contains(t, buf.String(), "Will remove: machine1")
	assert.Contains(t, buf.String(), "Will remove: machine2")
	assert.NotContains(t, buf.String(), `Error while removing machine`)

	assert.Equal(t, int64(0), cleaner.totalNumberOfRemovedMachines)
	assert.Equal(t, int64(0), cleaner.totalNumberOfRemoveMachineErrors)
}

func testClearRemovableMachinesWithErrorOnDelete(t *testing.T, buf *bytes.Buffer) {
	mf := &MockMachinesFinderInterface{}
	defer mf.AssertExpectations(t)
	machine1 := &MockMachineInterface{}
	defer machine1.AssertExpectations(t)
	machine2 := &MockMachineInterface{}
	defer machine2.AssertExpectations(t)

	modifiedAt := time.Now().Add(-20 * time.Second)

	machine1.On("DropletID").Once().Return(int64(0))
	machine1.On("ModifiedAt").Once().Return(modifiedAt)
	machine1.On("Name").Once().Return("machine1")
	machine1.On("ModifiedAt").Once().Return(modifiedAt)
	machine1.On("Delete").Once().Return(nil)

	machine2.On("DropletID").Once().Return(int64(0))
	machine2.On("ModifiedAt").Once().Return(modifiedAt)
	machine2.On("Name").Once().Return("machine2")
	machine2.On("ModifiedAt").Once().Return(modifiedAt)
	machine2.On("Name").Once().Return("machine2")
	machine2.On("Delete").Once().Return(fmt.Errorf("test error"))

	machines := []MachineInterface{
		machine1,
		machine2,
	}
	mf.On("ListMachines").Once().Return(machines, nil)

	cleaner := NewDropletZeroMachinesCleaner(mf, 10)
	cleaner.EnableDelete()
	cleaner.Clean()

	assert.Contains(t, buf.String(), "Starting machines cleanup")
	assert.Contains(t, buf.String(), "Number of found machines: 2")
	assert.Contains(t, buf.String(), "Finished machines cleanup. Removed 1 machines")
	assert.Contains(t, buf.String(), "Will remove: machine1")
	assert.Contains(t, buf.String(), "Will remove: machine2")
	assert.Contains(t, buf.String(), `Error while removing machine 'machine2': test error`)

	assert.Equal(t, int64(1), cleaner.totalNumberOfRemovedMachines)
	assert.Equal(t, int64(1), cleaner.totalNumberOfRemoveMachineErrors)
}

func testClearNotRemovableMachines(t *testing.T, buf *bytes.Buffer) {
	mf := &MockMachinesFinderInterface{}
	defer mf.AssertExpectations(t)
	machine1 := &MockMachineInterface{}
	defer machine1.AssertExpectations(t)
	machine2 := &MockMachineInterface{}
	defer machine2.AssertExpectations(t)

	machine1.On("DropletID").Once().Return(int64(10))

	machine2.On("DropletID").Once().Return(int64(0))
	machine2.On("ModifiedAt").Once().Return(time.Now().Add(-2 * time.Second))

	machines := []MachineInterface{
		machine1,
		machine2,
	}
	mf.On("ListMachines").Once().Return(machines, nil)

	cleaner := NewDropletZeroMachinesCleaner(mf, 10)
	cleaner.EnableDelete()
	cleaner.Clean()

	assert.Contains(t, buf.String(), "Starting machines cleanup")
	assert.Contains(t, buf.String(), "Number of found machines: 2")
	assert.Contains(t, buf.String(), "Finished machines cleanup. Removed 0 machines")
	assert.NotContains(t, buf.String(), "Will remove: machine1")
	assert.NotContains(t, buf.String(), "Will remove: machine2")
	assert.NotContains(t, buf.String(), `Error while removing machine`)

	assert.Equal(t, int64(0), cleaner.totalNumberOfRemovedMachines)
	assert.Equal(t, int64(0), cleaner.totalNumberOfRemoveMachineErrors)
}

func TestDropletZeroMachinesCleaner_Clean(t *testing.T) {
	tests := []func(*testing.T, *bytes.Buffer){
		testClearNoMachines,
		testClearRemovableMachines,
		testClearRemovableMachinesWithDeleteDisabled,
		testClearRemovableMachinesWithErrorOnDelete,
		testClearNotRemovableMachines,
	}

	for _, test := range tests {
		t.Run(getTestFuncName(test), func(t *testing.T) {
			runOnLogrus(t, func(t *testing.T, buf *bytes.Buffer) {
				test(t, buf)
			})
		})
	}
}
