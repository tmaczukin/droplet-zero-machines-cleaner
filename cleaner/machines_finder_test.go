package cleaner

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func testListMachinesOnEmptyDir(t *testing.T, buf *bytes.Buffer, directory string) {
	mf := MachinesFinder{machinesDirectory: directory}
	machines, err := mf.ListMachines()
	assert.NoError(t, err)
	assert.Empty(t, machines)
	assert.Empty(t, buf.String())
}

func testListMachinesWithMachineWithoutConfig(t *testing.T, buf *bytes.Buffer, directory string) {
	machineDir1, err := ioutil.TempDir(directory, "test-machine-")
	require.NoError(t, err)
	defer os.RemoveAll(machineDir1)

	machinePathParts := strings.Split(machineDir1, "/")
	machineName := machinePathParts[len(machinePathParts)-1]

	mf := MachinesFinder{machinesDirectory: directory}
	machines, err := mf.ListMachines()

	assert.NoError(t, err)
	assert.Empty(t, machines)
	assert.Contains(t, buf.String(), fmt.Sprintf("Couldn't find 'config.json' file for machine '%s'", machineName))
}

func testListMachinesWithMachine(t *testing.T, buf *bytes.Buffer, directory string) {
	machineDir1, err := ioutil.TempDir(directory, "test-machine-")
	require.NoError(t, err)
	defer os.RemoveAll(machineDir1)

	configFile := filepath.Join(machineDir1, "config.json")
	content := `{
  "Driver": {
    "DropletID": 123
  },
  "Name": "test-machine"
}`
	err = ioutil.WriteFile(configFile, []byte(content), os.ModePerm)
	require.NoError(t, err)

	info, err := os.Stat(configFile)
	require.NoError(t, err)

	info.ModTime()

	mf := MachinesFinder{machinesDirectory: directory}
	machines, err := mf.ListMachines()

	assert.NoError(t, err)
	assert.Empty(t, buf.String())
	require.Len(t, machines, 1)
	assert.Equal(t, "test-machine", machines[0].Name())
	assert.Equal(t, int64(123), machines[0].DropletID())
	assert.Equal(t, info.ModTime(), machines[0].ModifiedAt())
	assert.Equal(t, machineDir1, machines[0].Directory())
}

func TestMachinesFinder_ListMachines(t *testing.T) {
	tests := []func(*testing.T, *bytes.Buffer, string){
		testListMachinesOnEmptyDir,
		testListMachinesWithMachineWithoutConfig,
		testListMachinesWithMachine,
	}

	for _, test := range tests {
		t.Run(getTestFuncName(test), func(t *testing.T) {
			runOnLogrus(t, func(t *testing.T, buf *bytes.Buffer) {
				runOnTestMachinesDirectory(t, func(t *testing.T, directory string) {
					test(t, buf, directory)
				})
			})
		})
	}
}
