package cleaner

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func testMachineDeleteOnEmptyDir(t *testing.T, buf *bytes.Buffer, directory string) {
	machine := Machine{directory: directory}
	os.RemoveAll(directory)

	err := machine.Delete()
	assert.Regexp(t, "Deleting .* directory", buf.String())
	assert.NoError(t, err)
}

func testMachineDeleteOnNormalDir(t *testing.T, buf *bytes.Buffer, directory string) {
	machine := Machine{directory: directory}
	err := machine.Delete()
	assert.Regexp(t, "Deleting .* directory", buf.String())
	assert.NoError(t, err)
}

func TestMachine_Delete(t *testing.T) {
	tests := []func(*testing.T, *bytes.Buffer, string){
		testMachineDeleteOnEmptyDir,
		testMachineDeleteOnNormalDir,
	}

	for _, test := range tests {
		t.Run(getTestFuncName(test), func(t *testing.T) {
			runOnLogrus(t, func(t *testing.T, buf *bytes.Buffer) {
				runOnTestMachineDirectory(t, func(t *testing.T, directory string) {
					test(t, buf, directory)
				})
			})
		})
	}
}
