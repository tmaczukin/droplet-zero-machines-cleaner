package cleaner

import (
	"os"
	"time"

	"github.com/Sirupsen/logrus"
)

type MachineInterface interface {
	Delete() error

	Name() string
	DropletID() int64
	ModifiedAt() time.Time
	Directory() string
}

type Machine struct {
	name       string
	dropletID  int64
	modifiedAt time.Time
	directory  string
}

func (m *Machine) Delete() error {
	logrus.Warnf("Deleting '%s' directory", m.directory)
	return os.RemoveAll(m.directory)
}

func (m *Machine) Name() string { return m.name }

func (m *Machine) DropletID() int64 { return m.dropletID }

func (m *Machine) ModifiedAt() time.Time { return m.modifiedAt }

func (m *Machine) Directory() string { return m.directory }
