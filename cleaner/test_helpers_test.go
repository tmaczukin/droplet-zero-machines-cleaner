package cleaner

import (
	"bytes"
	"io/ioutil"
	"os"
	"reflect"
	"runtime"
	"strings"
	"testing"

	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

func getTestFuncName(test interface{}) string {
	fullName := runtime.FuncForPC(reflect.ValueOf(test).Pointer()).Name()
	nameParts := strings.Split(fullName, ".")

	return nameParts[len(nameParts)-1]
}

func runOnLogrus(t *testing.T, test func(*testing.T, *bytes.Buffer)) {
	out := logrus.StandardLogger().Out
	defer logrus.SetOutput(out)

	buf := &bytes.Buffer{}
	logrus.SetOutput(buf)

	test(t, buf)
}

func runOnTestMachinesDirectory(t *testing.T, test func(*testing.T, string)) {
	directory, err := ioutil.TempDir("", "machines-directory-")
	require.NoError(t, err)
	defer os.RemoveAll(directory)

	test(t, directory)
}

func runOnTestMachineDirectory(t *testing.T, test func(*testing.T, string)) {
	directory, err := ioutil.TempDir("", "test-machine-directory-")
	require.NoError(t, err)
	defer os.RemoveAll(directory)

	test(t, directory)
}
