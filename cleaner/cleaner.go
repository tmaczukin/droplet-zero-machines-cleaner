package cleaner

import (
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	numberOfRemovedMachines = prometheus.NewDesc(
		"droplet_zero_machines_cleaner_removed_machines_total",
		"Total number of removed machines",
		[]string{},
		nil,
	)

	numberOfRemoveMachineErrors = prometheus.NewDesc(
		"droplet_zero_machines_cleaner_remove_machine_errors_total",
		"Total number of machine removing errors",
		[]string{},
		nil,
	)
)

type DropletZeroMachinesCleaner struct {
	machinesFinder MachinesFinderInterface

	delete     bool
	machineAge time.Duration

	totalNumberOfRemovedMachines     int64
	totalNumberOfRemoveMachineErrors int64
}

func (c *DropletZeroMachinesCleaner) Describe(ch chan<- *prometheus.Desc) {
	ch <- numberOfRemovedMachines
	ch <- numberOfRemoveMachineErrors
}

func (c *DropletZeroMachinesCleaner) Collect(ch chan<- prometheus.Metric) {
	ch <- prometheus.MustNewConstMetric(
		numberOfRemovedMachines,
		prometheus.CounterValue,
		float64(c.totalNumberOfRemovedMachines),
	)
	ch <- prometheus.MustNewConstMetric(
		numberOfRemoveMachineErrors,
		prometheus.CounterValue,
		float64(c.totalNumberOfRemoveMachineErrors),
	)
}

func (c *DropletZeroMachinesCleaner) shouldRemoveMachine(machine MachineInterface) bool {
	if machine.DropletID() > 0 {
		return false
	}

	if time.Now().Sub(machine.ModifiedAt()) < c.machineAge {
		return false
	}

	return true
}

func (c *DropletZeroMachinesCleaner) deleteMachine(machine MachineInterface) {
	logrus.Infof("Will remove: %s (updated_at: %s)", machine.Name(), machine.ModifiedAt())
	if !c.delete {
		return
	}

	if err := machine.Delete(); err != nil {
		c.totalNumberOfRemoveMachineErrors++
		logrus.Errorf("Error while removing machine '%s': %v", machine.Name(), err)
		return
	}

	c.totalNumberOfRemovedMachines++
}

func (c *DropletZeroMachinesCleaner) deleteMachines(machines []MachineInterface) int64 {
	removed := c.totalNumberOfRemovedMachines
	for _, machine := range machines {
		if !c.shouldRemoveMachine(machine) {
			continue
		}

		c.deleteMachine(machine)
	}

	return c.totalNumberOfRemovedMachines - removed
}

func (c *DropletZeroMachinesCleaner) Clean() error {
	var count int64

	logrus.Infoln("Starting machines cleanup")
	defer func() {
		logrus.Infof("Finished machines cleanup. Removed %d machines", count)
	}()

	machines, err := c.machinesFinder.ListMachines()
	if err != nil {
		return err
	}
	logrus.Infof("Number of found machines: %d", len(machines))

	count = c.deleteMachines(machines)

	return nil
}

func (c *DropletZeroMachinesCleaner) EnableDelete() {
	c.delete = true
}

func NewDropletZeroMachinesCleaner(machinesFinder MachinesFinderInterface, machineAge int) *DropletZeroMachinesCleaner {
	ma := time.Duration(machineAge) * time.Second
	logrus.Infof("Machine minimal age: %s", ma)

	cleaner := &DropletZeroMachinesCleaner{
		machinesFinder: machinesFinder,
		machineAge:     ma,
	}

	return cleaner
}
