# Droplet Zero Machies Cleaner for GitLab Runner with authoscale using DigitalOcean

This tool looks for Docker Machine machines that failed on the creation step and
ended with `DropletID: 0` entry in the configuration file. Such machine cannot be
then removed by Runner automatically. This ends in wasted limit of machines that
Runner may manage.

This tool:
- lists machines existing on Runner's host,
- removes any machine directory, that contains `Droplet: 0` entry in the
  configuration file.

Additionally it's possible to enable metrics HTTP endpoint that allows monitoring
systems to track how many machines are being cleaned-up.

## Usage

### The `service` mode

In this mode tool is started as a service that works continuously and executed
a cleanup in configured intervals.

The tool requires few settings that should be provided. Notice that some of settings
can be also provided with environment variables:

| Setting              | Env                  | Required | Default value                | Description |
|----------------------|----------------------|----------|------------------------------|-------------|
| `machines-directory` | `MACHINES_DIRECTORY` | no       | `~/.docker/machine/machines` | Directory where Docker Machine stores configuration of created machines. This is used to list existing machines. **Must be an absolute path!** |
| `machine-age`        | `MACHINE_AGE`        | no       | `3600`                       | Minimal age of machine that can be removed. Counted from last modification of machine's configuration file. |
| `interval`           | `INTERVAL`           | no       | `3600`                       | Interval between subsequent cleanup attempts. Provided in seconds. |
| `listen`             | `LISTEN`             | no       | -                            | Address on which metrics server is started. If empty, then the feature is disabled. Provided in form of `1.2.3.4:1234` |

**Example**

```bash
$ ./droplet-zero-machines-cleaner service \
                                  --listen 0.0.0.0:9380 \
                                  --interval 900
```

### The `one-shot` mode

In this mode tool executes one cleanup attempt and exits. Additionally it doesn't
do a real cleanup by default. This mode can be used if someone wants only to list
the number of machines that should be removed.

With additional flag it can also remove droplets.

| Setting              | Env                  | Required | Default value                | Description |
|----------------------|----------------------|----------|------------------------------|-------------|
| `machines-directory` | `MACHINES_DIRECTORY` | no       | `~/.docker/machine/machines` | Directory where Docker Machine stores configuration of created machines. This is used to list existing machines. |
| `machine-age`        | `MACHINE_AGE`        | no       | `3600`                       | Minimal age of machine that can be removed. Counted from last modification of machine's configuration file. |
| `delete`             | -                    | no       | `false`                      | If provided the tool will do a real cleanup and remove droplets from DigitalOcean |

**Examples**

```bash
# To only list machines that should be removed
$ ./droplet-zero-machines-cleaner one-shot

# To list and remove machines
$ ./droplet-zero-machines-cleaner one-shot \
                             --delete
```

### Using Docker container

Prepared Docker image is configured to run the tool in `service` mode. It also starts the
metrics server by default.

When starting this as Docker container we need to add some additional configuration.

First - we need to mount host's machines directory to the container. With a default
configuration the process inside of Docker containers assumes, that the `machines-directory`
is set to `/machines`. In that case we need to use the `-v /path/to/hosts/machines/:/machines`.

If we want to access metrics server from an external monitoring system, then we should
also bind container's port to some host's port, e.g. `-p 9381:9381` which will bind
container's `9381` port to host's `9381` port on all host's interfaces. Notice that
if you want to access this port from an external machine you will probably need also
to update your firewall.

**Example**

```bash
$ docker run -d \
         --restart always \
         --name droplet_zero_machines_cleaner \
         --log-driver=syslog \
         --log-opt tag=droplet_zero_machines_cleaner \
         -e NO_COLOR=true \
         -v /root/.docker/machine/machines/:/machines \
         -p 9381:9381 \
         registry.gitlab.com/tmaczukin/droplet-zero-machines-cleaner:0.1
```

## Author

Tomasz Maczukin, 2017, GitLab

## License

MIT
