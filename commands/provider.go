package commands

import (
	"github.com/urfave/cli"

	"gitlab.com/tmaczukin/droplet-zero-machines-cleaner/cleaner"
)

const (
	DefaultInterval int = 3600
)

type CleanerProvider struct{}

func (s *CleanerProvider) GetCleaner(context *cli.Context) *cleaner.DropletZeroMachinesCleaner {
	return cleaner.NewDropletZeroMachinesCleaner(
		cleaner.NewMachinesFinder(context.String("machines-directory")),
		context.Int("machine-age"),
	)
}

func (s *CleanerProvider) Flags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   "machines-directory",
			Usage:  "Absolute path to directory where Docker Machine machines configuration is stored",
			Value:  cleaner.GetDefaultMachinesDirectory(),
			EnvVar: "MACHINES_DIRECTORY",
		},
		cli.IntFlag{
			Name:   "machine-age",
			Usage:  "Minimal age of machine that can be removed",
			Value:  DefaultInterval,
			EnvVar: "MACHINE_AGE",
		},
	}
}
