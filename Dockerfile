FROM alpine:3.6

ENV LISTEN 0.0.0.0:9381
ENV MACHINES_DIRECTORY /machines

EXPOSE 9381

RUN apk add -U ca-certificates; \
    mkdir -p $MACHINES_DIRECTORY

COPY ./droplet-zero-machines-cleaner /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/droplet-zero-machines-cleaner", "service"]
